-- | The master `Main` module
module Main where
import System.Environment (getArgs)

import qualified Test.DMuCheck.Master as Master
import qualified Test.DMuCheck.DAdapter as D
import qualified Test.MuCheck.TestAdapter as A

-- | Host and port are baked in for now.
localHost, localPort :: String
localHost = "127.0.0.1"
localPort = "10501"

-- | Help text
help :: IO ()
help = do putStrLn "d-master <file>"
          putStrLn "E.g."
          putStrLn "  d-master Examples/QuickCheckTest.hs"

-- | The main program, handing over control to the `Master` module
main :: IO ()
main = do
  args <- getArgs
  case args of
    ("-h":_) -> help
    (file : _) -> Master.process localHost localPort (D.getM (A.toRun file))
    _ -> putStrLn "use -h to get help."

