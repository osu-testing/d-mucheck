-- | The `Main` module for simple forking mucheck
module Main where
import System.Environment (getArgs)
import System.Posix.Process

import qualified Test.DMuCheck.Slave as Slave
import qualified Test.DMuCheck.Master as Master
import qualified Test.DMuCheck.DAdapter as D
import qualified Test.MuCheck.TestAdapter as A

import qualified Usage

import Control.Monad

-- | The host and the port are baked in.
localHost, localPort :: String
localHost = "127.0.0.1"
localPort = "12345"

-- | The main program
main :: IO ()
main = do
  args <- getArgs
  case args of
    ("-h":_) -> Usage.help
    (sslaves : file : _ ) -> process sslaves file
    _ -> putStrLn "use -h to get help."

-- | The `process` starts both master and also the rquired number of slaves as
-- child processes.
process :: String -> String -> IO ()
process sslaves file = do
  let host = localHost
      nport = read localPort :: Int
      nslaves = read sslaves :: Int

  forM_ [show (i + nport) | i <- [1..nslaves]] (forkProcess . Slave.process host) 

  Master.process localHost localPort (D.getM (A.toRun file))

