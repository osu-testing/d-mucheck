-- | The `Help` module
module Main where
import Data.List (unlines)

-- | A place holdeer executable for when no options are chose. It also provides
-- some help text
main :: IO ()
main = putStrLn (unlines
        ["DMuCheck provides three main executables, d-mucheck, d-master, and d-slave.",
        "These executables are again built against one of each supported testing ",
        "frameworks - QuickCheck, SmallCheck, HUnit, Hspec. Depending on your site,",
        "enable one of these ",
        "(E.g `cabal configure -fQuickCheck`; cabal build; cabal install)",
        "Note that the same binaries get rewritten for each test framework.",
        "The `d-mucheck` is the simplest of all, and only provides a simple",
        "forking parallelization of mutation analysis.",
        "The `d-master` and `d-slave` pair is used for client-server distribution",
        "across machine boundaries. You can invoke `d-slave` multiple times. Be sure",
        "to use different ports if it is invoked multiple times from the same machine"
        ])
