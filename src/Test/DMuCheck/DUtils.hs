{-# LANGUAGE BangPatterns, TemplateHaskell, DeriveGeneric, DeriveDataTypeable, StandaloneDeriving, TypeSynonymInstances #-}
-- | The utilities module.
module Test.DMuCheck.DUtils where

import Control.Distributed.Process
import Control.Distributed.Process.Closure
import Control.Distributed.Process.Backend.SimpleLocalnet
import Control.Distributed.Process.Node (initRemoteTable)

import GHC.Generics
import Data.Binary

import Control.Monad
import Control.Concurrent
import Data.List
import Data.Typeable

import Test.MuCheck.TestAdapter
import Test.MuCheck.Interpreter
import Test.MuCheck.AnalysisSummary
import Test.MuCheck.Tix
import Test.MuCheck.Config

import Test.DMuCheck.DAdapter

deriving instance Typeable Mutant
deriving instance Typeable Span
deriving instance Typeable MuVar

deriving instance Generic Summary
deriving instance Generic MutantSummary
deriving instance Generic Mutant
deriving instance Generic Span
deriving instance Generic MuVar
instance Binary Summary
instance Binary MutantSummary
instance Binary Mutant
instance Binary Span
instance Binary MuVar

-- | The slave process. It asks the master for work, and for any mutant
-- reuturned, evaluates and sends back the summarized results. Note that the
-- summarizeResults function is from `DAdapter` module which has different
-- implementations depending on which flag was enabled during compilation.
slave :: (ProcessId, ProcessId) -> Process ()
slave (_master, workQueue) = do
    self <- getSelfPid
    askForWork self
  where
    askForWork self = do
      send workQueue self
      receiveWait
        [
          match $ \(sender, modFile, mutant, tests) -> do
            ioResults <- liftIO $ evalMutant tests mutant
            let val = summarizeResults (getM modFile) tests (mutant, ioResults)
            send sender val
            askForWork self
        , match $ \() -> return ()
        ]

remotable ['slave]

rtable :: RemoteTable
rtable = __remoteTable initRemoteTable
-- | Slave closure
slaveC :: (ProcessId, ProcessId) -> Closure (Process ())
slaveC = $(mkClosure 'slave)

-- | Wait for slaves to register, and then spawn the mutation analysis on new
-- slaves
initSlave :: Backend -> (ProcessId, ProcessId) -> [ProcessId] -> Process [ProcessId]
initSlave backend (us, workQueue) slaves = do
  myslaves <- findSlaves backend
  let newslaves = myslaves \\ slaves
      secs n = n * 1000000
  unless (null newslaves ) $
    liftIO $ putStrLn $ "registered: " ++ show newslaves
  redirectLogsHere backend newslaves
  forM_ newslaves $ \nid -> spawn (processNodeId nid) $ slaveC (us, workQueue)
  liftIO $ threadDelay $ secs 1
  initSlave backend (us, workQueue) myslaves

-- | Wait for all mutants and return back the collected list.
collectMutants :: Int -> Process [MutantSummary]
collectMutants nAll = collectOne [] nAll
  where
    collectOne :: [MutantSummary] -> Int -> Process [MutantSummary]
    collectOne !acc 0 = return acc
    collectOne !acc n = do
      m <- expect
      collectOne (m:acc) (n - 1)

-- | Display results from `collectMutants`
showResults :: [Mutant] -> Process ()
showResults mutants = do
    res <- collectMutants (length mutants)
    let ma = MAnalysisSummary {
      _maOriginalNumMutants = -1,
      _maCoveredNumMutants = -1,
      _maNumMutants = length mutants,
      _maAlive = length [m | MSumAlive m _ <- res],
      _maKilled = length [m | MSumKilled m _ <- res],
      _maErrors = length [m | MSumOther m _ <- res]}
    say (show ma)

