-- | The `Slave` module. It has one job, to start the slave on a given host and
-- port.
module Test.DMuCheck.Slave where
import Control.Distributed.Process.Backend.SimpleLocalnet

import qualified Test.DMuCheck.DUtils as DUtils

-- | Starts the slave process on a given host and port
process :: String -> String -> IO ()
process host port = do
  backend <- initializeBackend host port DUtils.rtable
  startSlave backend

