{-# LANGUAGE RecordWildCards #-}
-- | The Master module. It initiates a workqueue process that slaves can
-- connect and request for mutants to evaluate. It also checkes for newly born
-- slaves, and spawns the slave closure on these.
module Test.DMuCheck.Master where
import Control.Distributed.Process
import Control.Distributed.Process.Backend.SimpleLocalnet

import qualified Test.DMuCheck.DUtils as DUtils

import Control.Monad
import Data.Binary
import Data.Typeable

import Test.MuCheck
import Test.MuCheck.Utils.Common
import Test.MuCheck.Mutation
import Test.MuCheck.Config
import Test.MuCheck.TestAdapter


-- | The `process` initiates the work queue, and waits for slaves to connect
-- and request work. It also periodically searches for new slaves and spawns
-- the slave closure on these. 
process :: (Typeable a, Binary a, Show b, Summarizable b, TRun a b) => String -> String -> a -> IO ()
process host port moduleFile = do
  (len, mutants) <- genMutants (getName moduleFile) []
  smutants <- sampler defaultConfig mutants
  tests <- getAllTests (getName moduleFile)

  backend <- initializeBackend host port DUtils.rtable
  startMaster backend $ \_slaves -> do
    us <- getSelfPid
    workQueue <- spawnLocal $ do
      forM_ mutants $ \mutant -> do
        them <- expect
        send them (us, moduleFile, mutant, (map (genTest moduleFile) tests))
      forever $ do
        them <- expect
        send them ()

    _slaver <- spawnLocal $ forever $ DUtils.initSlave backend (us, workQueue) []

    DUtils.showResults mutants
    terminateAllSlaves backend

