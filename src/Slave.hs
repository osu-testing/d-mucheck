-- | The `Main` module for slave
module Main where
import System.Environment (getArgs)

import qualified Test.DMuCheck.Slave as Slave

-- | The host is baked in, while port is accepted from the user
localHost :: String
localHost = "127.0.0.1"

-- | The help text
help :: IO ()
help = do putStrLn "d-slave <port>"
          putStrLn "E.g."
          putStrLn "  d-slave 12345"

-- | The main program, handing over control to `Slave` module
main :: IO ()
main = do
  args <- getArgs
  case args of
    ("-h":_) -> help
    (port : _args) -> Slave.process localHost port
    _ -> putStrLn "use -h to get help."

