module Examples.HUnitTest where
import Test.HUnit

qsort :: Ord a => [a] -> [a]
qsort [] = []
qsort (x:xs) = qsort l ++ [x] ++ qsort r
        where l = filter (<=x) xs
              r = filter (>x) xs

uncoveredDummy :: Int -> Int
uncoveredDummy a = 0 + a

{-# ANN test1 "Test" #-}
test1 = toTest "test1" (assertEqual "for (qsort [3,2])" [2,3] (qsort [3,2]))

{-# ANN test2 "Test" #-}
test2 = toTest "test2" (assertEqual "for (qsort [3,2,1])" [1,2,3] (qsort [3,2,1]))

{-# ANN test3 "Test" #-}
test3 = toTest "test3" (assertEqual "for (qsort [3,2,1,4])" [1,2,3,4] (qsort [3,2,1,4]))

{-# ANN toTest "TestSupport" #-}
toTest s f = TestList[TestLabel s $ TestCase f]

{-
-- You can also do this, but then, you cant rely on the mutation analysis stoping
-- on the first failure of a mutant.

{-# ANN test1 "TestSupport" #-}
test1 = TestCase (assertEqual "for (qsort [3,2])" [2,3] (qsort [3,2]))
{-# ANN test2 "TestSupport" #-}
test2 = TestCase (assertEqual "for (qsort [3,2,1])" [1,2,3] (qsort [3,2,1]))
{-# ANN test3 "TestSupport" #-}
test3 = TestCase (assertEqual "for (qsort [3,2,1,4])" [1,2,3,4] (qsort [3,2,1,4]))

{-# ANN tests "Test" #-}
tests = TestList [TestLabel "test1" test1
                , TestLabel "test2" test2
                , TestLabel "test3" test3]
-}
