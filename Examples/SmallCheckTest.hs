module Examples.SmallCheckTest where
import Data.List
import Control.Exception
import Control.Monad
import Test.SmallCheck.Drivers

qsort :: [Int] -> [Int]
qsort [] = []
qsort (x:xs) = qsort l ++ [x] ++ qsort r
    where l = filter (< x) xs
          r = filter (>= x) xs

uncoveredDummy :: Int -> Int
uncoveredDummy a = 0 + a

{-# ANN idEmpProp "Test" #-}
idEmpProp xs = qsort xs == qsort (qsort xs)

{-# ANN revProp "Test" #-}
revProp xs = qsort xs == qsort (reverse xs)

{-# ANN modelProp "Test" #-}
modelProp xs = qsort xs == sort xs

-- Supports
{-# ANN catchAny "TestSupport" #-}
catchAny :: IO a -> (SomeException -> IO a) -> IO a
catchAny = Control.Exception.catch

{-# ANN smallCheckResult "TestSupport" #-}
smallCheckResult v = catch (smallCheckM 5 v) handleErr

{-# ANN handleErr "TestSupport" #-}
handleErr :: SomeException -> IO (Maybe PropertyFailure)
handleErr e = return $ Just (PropertyFalse (Just (show e)))

