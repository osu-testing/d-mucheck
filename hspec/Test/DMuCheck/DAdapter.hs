{-# LANGUAGE StandaloneDeriving, DeriveDataTypeable, DeriveGeneric #-}
module Test.DMuCheck.DAdapter where
import Data.Binary
import Data.Typeable
import GHC.Generics


import Test.MuCheck.TestAdapter
import Test.MuCheck.TestAdapter.Hspec

deriving instance Typeable HspecRun
deriving instance Generic HspecRun
instance Binary HspecRun

getM :: HspecRun -> HspecRun
getM = id

