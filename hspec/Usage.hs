-- | The `Usage` module for simple forking mucheck
module Usage where

-- | The help text
help :: IO ()
help = do putStrLn "d-mucheck <number of slaves> <file>"
          putStrLn "E.g."
          putStrLn "  d-mucheck 4 Examples/HspecTest.hs"

