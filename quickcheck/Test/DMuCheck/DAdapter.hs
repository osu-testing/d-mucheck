{-# LANGUAGE StandaloneDeriving, DeriveDataTypeable, DeriveGeneric #-}
module Test.DMuCheck.DAdapter where
import Data.Binary
import Data.Typeable
import GHC.Generics

import Test.MuCheck.TestAdapter
import Test.MuCheck.TestAdapter.QuickCheck

deriving instance Typeable QuickCheckRun
deriving instance Generic QuickCheckRun
instance Binary QuickCheckRun

getM :: QuickCheckRun -> QuickCheckRun
getM = id

