# README

DMuCheck provides a simple wrapper over the `MuCheck` library, to accomplish
parallel mutation analysis of Haskell programs with tests written using the
supported test frameworks. It provides two ways of doing it, first the simple
forked processes from the main process, and second, using a master and multiple
slaves.

It supports the test frameworks QuickCheck, SmallCheck, HUnit and HSpec, which
are enabled by using flags. The flags available are
`-fQuickCheck, -fSmallCheck, -fHUnit, -fHspec`.

You can only use one of these flags at a time.

## Using QuickCheck

First build it using the flag `-fQuickCheck`
```
cabal configure -fQuickCheck
cabal build
```

### Simple

Simple uses a single machine, and spawns multiple processes to run mutation
analysis parallelly.  Start the mutation using the simple client

```
 ./dist/build/d-mucheck/d-mucheck 4 Examples/QuickCheckTest.hs
```

This will start 4 processes that will evaluate all `13` mutants from `Examples/QuickCheckTest.hs` for you. The mutants can be found under `.mutants` directory.

### Advanced

The advanced option is to use a separate master and slave clients to do the
mutation. The advantage is that the slaves can be in any machines in the local
network.

First, start your master

```
 ./dist/build/d-master/d-master Examples/SmallCheckTest.hs
```
Then in separate terminals (or in machines), execute these
```
./dist/build/d-slave/d-slave 12346
```
```
./dist/build/d-slave/d-slave 12347
```
```
./dist/build/d-slave/d-slave 12348
```
You can start any number of slaves, and you will see slaves registering on the
master server. We use workstealing, so slaves request work when they join. For
now, you cant bring down any slave once started. The slaves exit when the
master finishes.

See Makefile at [src](https://bitbucket.org/osu-testing/d-mucheck/src/) for more examples in HUnit, HSpec and SmallCheck.