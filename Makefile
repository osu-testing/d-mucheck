hlint:
	(cd support; ~/.cabal/bin/hlint `find ../src -name \*.hs`)

clean-sandbox:
	- cabal sandbox hc-pkg unregister MuCheck-QuickCheck

sandbox:
	mkdir -p ../mucheck-sandbox
	cabal sandbox init --sandbox ../mucheck-sandbox

build:
	cabal sandbox init --sandbox ../mucheck-sandbox 
	cabal build

build-all:
	cabal sandbox init --sandbox ../mucheck-sandbox 
	for i in QuickCheck SmallCheck HUnit Hspec; do echo building $$i; cabal configure -f$$i; cabal build; cp -r dist/build dist/build.$$i; done

.PHONY: quickcheck smallcheck hunit hspec

quickcheck:
	cabal sandbox init --sandbox ../mucheck-sandbox 
	cabal configure -fQuickCheck
	cabal build d-mucheck
	env MuDEBUG=1 ./dist/build/d-mucheck/d-mucheck 2 Examples/QuickCheckTest.hs

# will work only if -fQuickCheck was used for building
quickcheck-run:
	./dist/build/d-mucheck/d-mucheck 4 Examples/QuickCheckTest.hs

quickcheck-master:
	./dist/build/d-master/d-master Examples/QuickCheckTest.hs

quickcheck-slave:
	./dist/build/d-slave/d-slave 12346

smallcheck:
	cabal sandbox init --sandbox ../mucheck-sandbox 
	cabal configure -fSmallCheck
	cabal build d-mucheck
	env MuDEBUG=1 ./dist/build/d-mucheck/d-mucheck 2 Examples/SmallCheckTest.hs

# will work only if -fSmallCheck was used for building
smallcheck-run:
	./dist/build/d-mucheck/d-mucheck 4 Examples/SmallCheckTest.hs

smallcheck-master:
	./dist/build/d-master/d-master Examples/SmallCheckTest.hs

smallcheck-slave:
	./dist/build/d-slave/d-slave 12346

hunit:
	cabal sandbox init --sandbox ../mucheck-sandbox 
	cabal configure -fHUnit
	cabal build d-mucheck
	env MuDEBUG=1 ./dist/build/d-mucheck/d-mucheck 2 Examples/HUnitTest.hs
# will work only if -fHUnit was used for building
hunit-run:
	./dist/build/d-mucheck/d-mucheck 4 Examples/HUnitTest.hs

hunit-master:
	./dist/build/d-master/d-master Examples/HUnitTest.hs

hunit-slave:
	./dist/build/d-slave/d-slave 12346


hspec:
	cabal sandbox init --sandbox ../mucheck-sandbox 
	cabal configure -fHspec
	cabal build d-mucheck
	env MuDEBUG=1 ./dist/build/d-mucheck/d-mucheck 2 Examples/HspecTest.hs

# will work only if -fHspec was used for building
hspec-run:
	./dist/build/d-mucheck/d-mucheck 4 Examples/HspecTest.hs

hspec-master:
	./dist/build/d-master/d-master Examples/HspecTest.hs

hspec-slave:
	./dist/build/d-slave/d-slave 12346

prepare:
	cabal haddock
	cabal check
	cabal sdist

clean:
	- rm Examples/*_*
	- rm *.log

