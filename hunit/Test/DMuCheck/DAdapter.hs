{-# LANGUAGE StandaloneDeriving, DeriveDataTypeable, DeriveGeneric #-}
module Test.DMuCheck.DAdapter where
import Data.Binary
import Data.Typeable
import GHC.Generics

import Test.MuCheck.TestAdapter
import Test.MuCheck.TestAdapter.HUnit

deriving instance Typeable HUnitRun
deriving instance Generic HUnitRun
instance Binary HUnitRun

getM :: HUnitRun -> HUnitRun
getM = id

