{-# LANGUAGE StandaloneDeriving, DeriveDataTypeable, DeriveGeneric #-}
module Test.DMuCheck.DAdapter where
import Data.Binary
import Data.Typeable
import GHC.Generics

import Test.MuCheck.TestAdapter
import Test.MuCheck.TestAdapter.SmallCheck

deriving instance Typeable SmallCheckRun
deriving instance Generic SmallCheckRun
instance Binary SmallCheckRun

getM :: SmallCheckRun -> SmallCheckRun
getM = id

